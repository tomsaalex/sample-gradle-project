package org.example.operations;

import org.example.exceptions.IllegalArithmeticOperationException;
import org.example.interfaces.IOperation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class MultiplicationOperationTest {

    @Test
    public void testExecute()
    {
        try {
            IOperation multiplicationOperation1 = new MultiplicationOperation(1, 3);
            IOperation multiplicationOperation2 = new MultiplicationOperation(-3, 3);
            IOperation multiplicationOperation3 = new MultiplicationOperation(3, 0);
            IOperation multiplicationOperation4 = new MultiplicationOperation(-0.5F, 2);
            IOperation multiplicationOperation5 = new MultiplicationOperation(-0.5F, -2);
            IOperation multiplicationOperation6 = new MultiplicationOperation(0.5F, 2);
            IOperation multiplicationOperation7 = new MultiplicationOperation(0, 0);

            assertEquals(multiplicationOperation1.execute(), 3);
            assertEquals(multiplicationOperation2.execute(), -9);
            assertEquals(multiplicationOperation3.execute(), 0);
            assertEquals(multiplicationOperation4.execute(), -1);
            assertEquals(multiplicationOperation5.execute(), 1);
            assertEquals(multiplicationOperation6.execute(), 1);
            assertEquals(multiplicationOperation7.execute(), 0);
        } catch (IllegalArithmeticOperationException e)
        {
            fail("This is not meant to throw an error");
        }
    }

}
