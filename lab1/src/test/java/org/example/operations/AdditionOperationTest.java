package org.example.operations;

import org.example.exceptions.IllegalArithmeticOperationException;
import org.example.interfaces.IOperation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class AdditionOperationTest {

    @Test
    public void testExecute()
    {
        IOperation additionOperation1 = new AdditionOperation(1, 2);
        IOperation additionOperation2 = new AdditionOperation(1, 0);
        IOperation additionOperation3 = new AdditionOperation(1, -2);

        try{

            assertEquals(additionOperation1.execute(), 3);
            assertEquals(additionOperation2.execute(), 1);
            assertEquals(additionOperation3.execute(), -1);

            assertNotEquals(additionOperation1.execute(), 10);
            assertNotEquals(additionOperation2.execute(), 10);
            assertNotEquals(additionOperation3.execute(), 10);

        } catch (IllegalArithmeticOperationException e)
        {
            fail("This is not meant to throw an error");
        }
    }

}
