package org.example.operations;

import org.example.exceptions.IllegalArithmeticOperationException;
import org.example.interfaces.IOperation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class MinOperationTest {

    @Test
    public void testExecute()
    {
        try {
            IOperation minOperation1 = new MinOperation(1, 3);
            IOperation minOperation2 = new MinOperation(-3, 3);
            IOperation minOperation3 = new MinOperation(0.5F, 0);
            IOperation minOperation4 = new MinOperation(-0.5F, -2);

            assertEquals(minOperation1.execute(), 1);
            assertEquals(minOperation2.execute(), -3);
            assertEquals(minOperation3.execute(), 0);
            assertEquals(minOperation4.execute(), -2);
        } catch (IllegalArithmeticOperationException e)
        {
            fail("This is not meant to throw an error");
        }
    }
}
