package org.example.operations;

import org.example.exceptions.IllegalArithmeticOperationException;
import org.example.interfaces.IOperation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class SubtractionOperationTest {

    @Test
    public void testExecute()
    {
        try{

            IOperation subtractionOperation1 = new SubtractionOperation(1, 2);
            IOperation subtractionOperation2 = new SubtractionOperation(1, 0);
            IOperation subtractionOperation3 = new SubtractionOperation(1, -2);

            assertEquals(subtractionOperation1.execute(), -1);
            assertEquals(subtractionOperation2.execute(), 1);
            assertEquals(subtractionOperation3.execute(), 3);

            assertNotEquals(subtractionOperation1.execute(), 10);
            assertNotEquals(subtractionOperation2.execute(), 10);
            assertNotEquals(subtractionOperation3.execute(), 10);


        } catch (
        IllegalArithmeticOperationException e)
        {
            fail("This is not meant to throw an error");
        }
    }
}
