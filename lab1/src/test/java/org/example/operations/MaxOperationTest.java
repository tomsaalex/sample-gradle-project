package org.example.operations;

import org.example.exceptions.IllegalArithmeticOperationException;
import org.example.interfaces.IOperation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class MaxOperationTest {

    @Test
    public void testExecute()
    {
        try {
            IOperation maxOperation1 = new MaxOperation(1, 3);
            IOperation maxOperation2 = new MaxOperation(-3, 3);
            IOperation maxOperation3 = new MaxOperation(0.5F, 0);
            IOperation maxOperation4 = new MaxOperation(-0.5F, -2);

            assertEquals(maxOperation1.execute(), 3);
            assertEquals(maxOperation2.execute(), 3);
            assertEquals(maxOperation3.execute(), 0.5);
            assertEquals(maxOperation4.execute(), -0.5);
        } catch (IllegalArithmeticOperationException e)
        {
            fail("This is not meant to throw an error");
        }

    }
}
