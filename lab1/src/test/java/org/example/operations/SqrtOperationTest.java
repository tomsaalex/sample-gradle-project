package org.example.operations;

import org.example.exceptions.IllegalArithmeticOperationException;
import org.example.interfaces.IOperation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SqrtOperationTest {


    @Test
    public void testExecute()
    {
        try {
            IOperation sqrtOperation1 = new SqrtOperation(1);
            IOperation sqrtOperation2 = new SqrtOperation(-3);
            IOperation sqrtOperation3 = new SqrtOperation(0);
            IOperation sqrtOperation4 = new SqrtOperation(0.5F);
            IOperation sqrtOperation5 = new SqrtOperation(3);
            IOperation sqrtOperation6 = new SqrtOperation(9);
            IOperation sqrtOperation7 = new SqrtOperation(100);

            assertThrows(IllegalArithmeticOperationException.class, sqrtOperation2::execute);

            assertEquals(sqrtOperation1.execute(), 1, 0.001);
            assertEquals(sqrtOperation3.execute(), 0, 0.001);
            assertEquals(sqrtOperation4.execute(), 0.7071, 0.001);
            assertEquals(sqrtOperation5.execute(), 1.732, 0.001);
            assertEquals(sqrtOperation6.execute(), 3, 0.001);
            assertEquals(sqrtOperation7.execute(), 10, 0.001);
        } catch (IllegalArithmeticOperationException e)
        {
            fail("This is not meant to throw an error");
        }
    }
}
