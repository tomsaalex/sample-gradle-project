package org.example.operations;

import org.example.exceptions.IllegalArithmeticOperationException;
import org.example.interfaces.IOperation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class DivisionOperationTest {

    @Test
    public void testExecute()
    {
        IOperation divisionOperation1 = new DivisionOperation(1, 2);
        IOperation divisionOperation2 = new DivisionOperation(1, 0);
        IOperation divisionOperation3 = new DivisionOperation(10, -2);
        IOperation divisionOperation4 = new DivisionOperation(10, 2);

        assertThrows(IllegalArithmeticOperationException.class, divisionOperation2::execute);

        try{
        assertEquals(divisionOperation1.execute(), 0.5);
        assertEquals(divisionOperation3.execute(), -5);
        assertEquals(divisionOperation4.execute(), 5);

        assertNotEquals(divisionOperation1.execute(), 10);
        assertNotEquals(divisionOperation3.execute(), 10);
        assertNotEquals(divisionOperation4.execute(), 10);
    } catch (IllegalArithmeticOperationException e)
        {

            fail("This is not meant to throw an error");
        }
    }
}
