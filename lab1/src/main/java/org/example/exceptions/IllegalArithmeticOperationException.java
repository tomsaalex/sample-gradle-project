package org.example.exceptions;

public class IllegalArithmeticOperationException extends Exception {
    public IllegalArithmeticOperationException() {
    }

    public IllegalArithmeticOperationException(String message) {
        super(message);
    }

    public IllegalArithmeticOperationException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalArithmeticOperationException(Throwable cause) {
        super(cause);
    }

    public IllegalArithmeticOperationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
