package org.example.operations;

import org.example.exceptions.IllegalArithmeticOperationException;
import org.example.interfaces.IDivisionOperation;

public class DivisionOperation implements IDivisionOperation {
    float num1, num2;

    public DivisionOperation(float num1, float num2) {
        this.num1 = num1;
        this.num2 = num2;
    }


    @Override
    public float execute() throws IllegalArithmeticOperationException {
        if(num2 == 0) throw new IllegalArithmeticOperationException("Cannot divide by zero");
        return num1 / num2;
    }
}
