package org.example.operations;

import org.example.interfaces.ISubtractionOperation;

public class SubtractionOperation implements ISubtractionOperation {
    float num1, num2;

    public SubtractionOperation(float num1, float num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    @Override
    public float execute() {
        return num1 - num2;
    }
}
