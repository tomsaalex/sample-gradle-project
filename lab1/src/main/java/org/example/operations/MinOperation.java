package org.example.operations;

import org.example.interfaces.IMinOperation;

public class MinOperation implements IMinOperation {

    float num1, num2;

    public MinOperation(float num1, float num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    @Override
    public float execute() {
        if(num1 <= num2)
            return num1;
        return num2;
    }
}
