package org.example.operations;

import org.example.interfaces.IMaxOperation;

public class MaxOperation implements IMaxOperation {
    float num1, num2;

    public MaxOperation(float num1, float num2) {
        this.num1 = num1;
        this.num2 = num2;
    }


    @Override
    public float execute() {
        if(num1 >= num2)
            return num1;
        return num2;
    }
}
