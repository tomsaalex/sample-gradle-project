package org.example.operations;

import org.example.exceptions.IllegalArithmeticOperationException;
import org.example.interfaces.ISqrtOperation;

public class SqrtOperation implements ISqrtOperation {
    float num1;

    public SqrtOperation(float num1) {
        this.num1 = num1;
    }

    @Override
    public float execute() throws IllegalArithmeticOperationException {
        if(num1 < 0) throw new IllegalArithmeticOperationException("Sqrt is not defined for negative numbers");
        return (float) Math.sqrt(num1);
    }
}
