package org.example.ui;

import org.example.exceptions.IllegalArithmeticOperationException;
import org.example.interfaces.IOperation;
import org.example.operations.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class CalculatorUI {
    private float baseOperand;
    private final BufferedReader consoleReader;

    public CalculatorUI() {
        this.baseOperand = 0;
        this.consoleReader = new BufferedReader(new InputStreamReader(System.in));
    }



    public IOperation resolveOperation(String operationText) throws Exception {
        String[] stringComponents = operationText.split(" ");
        String operandString = "";
        String operator = stringComponents[0];

        float newOperand = 0;
        if(!Objects.equals(operator, "sqrt")) {
            operandString = stringComponents[1];
            try {
                newOperand = Float.parseFloat(operandString);
            } catch (NumberFormatException e) {
                throw new NumberFormatException(operandString);
            }
        }

        switch (operator)
        {
            case "+": return new AdditionOperation(baseOperand, newOperand);
            case "-": return new SubtractionOperation(baseOperand, newOperand);
            case "*": return new MultiplicationOperation(baseOperand, newOperand);
            case "/": return new DivisionOperation(baseOperand, newOperand);
            case "min": return new MinOperation(baseOperand, newOperand);
            case "max": return new MaxOperation(baseOperand, newOperand);
            case "sqrt": return new SqrtOperation(baseOperand);
            default: throw new Exception("Unrecognized operator");
        }
    }


    public void render()
    {

            while(true)
            {
                try{
                    System.out.print("\n" + this.baseOperand + ": ");

                    String nextCommand = consoleReader.readLine();
                    if(Objects.equals(nextCommand, "exit"))
                        break;

                    IOperation nextOperation = resolveOperation(nextCommand);
                    baseOperand = nextOperation.execute();
                }
                catch (IOException e)
                {
                    System.err.println("An issue occurred while reading command");
                }
                catch (IndexOutOfBoundsException e)
                {
                    System.err.println("The command was not recognized");
                }
                catch (NumberFormatException e)
                {
                    System.err.println("The operand " + e.getMessage() + "is not recognized as a real number");
                } catch (IllegalArithmeticOperationException e)
                {
                    System.err.println(e.getMessage());
                }
                catch (Exception e) {
                    System.err.println(e.getMessage());
                }
            }


    }
}
