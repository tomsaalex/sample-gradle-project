package org.example.interfaces;

import org.example.exceptions.IllegalArithmeticOperationException;

public interface IOperation {
    float execute() throws IllegalArithmeticOperationException;
}
