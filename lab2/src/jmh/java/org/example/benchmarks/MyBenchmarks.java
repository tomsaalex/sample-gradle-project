package org.example.benchmarks;

import org.example.model.Order;
import org.example.model.RepositoryType;
import org.example.repository.InMemoryRepository;
import org.example.repository.implementations.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;


@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 5, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 2, time = 5, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class MyBenchmarks {


    @State(Scope.Benchmark)
    public static class EmptyRepoState {
        @Param({"ARRAY_LIST", "HASH_SET", "TREE_SET", "CONCURRENT_HASH_MAP", "EC_FAST_LIST", "KOLOBOKE_HASH_MAP", "FU_ARRAY_SET", "TROVE_HASH_SET"})
        String stringRepoType;

        InMemoryRepository<Order> repository;
        List<Order> orders;

        @Setup(Level.Invocation)
        public void doSetup() {
            RepositoryType repoType = RepositoryType.valueOf(stringRepoType);

            switch (repoType) {
                case ARRAY_LIST:
                    repository = new ArrayListBasedRepository<>();
                    break;
                case HASH_SET:
                    repository = new HashSetBasedRepository<>();
                    break;
                case TREE_SET:
                    repository = new TreeSetBasedRepository<>();
                    break;
                case CONCURRENT_HASH_MAP:
                    repository = new ConcurrentHashMapBasedRepository<>();
                    break;
                case EC_FAST_LIST:
                    repository = new EcFastListBasedRepository<>();
                    break;
                case KOLOBOKE_HASH_MAP:
                    repository = new KolobokeHashIntObjectBasedRepository<>();
                    break;
                case FU_ARRAY_SET:
                    repository = new FUArraySetBasedRepository<>();
                    break;
                case TROVE_HASH_SET:
                    repository = new TroveHashSetBasedRepository<>();
                    break;
            }

            orders = new ArrayList<>();

            Random random = new Random();
            for (int i = 0; i < 1000; i++) {
                orders.add(new Order(i, random.nextInt(100), random.nextInt(100)));
            }
        }
    }

    @State(Scope.Benchmark)
    public static class FullRepoStateContains {
        @Param({"ARRAY_LIST", "HASH_SET", "TREE_SET", "CONCURRENT_HASH_MAP", "EC_FAST_LIST", "KOLOBOKE_HASH_MAP", "FU_ARRAY_SET", "TROVE_HASH_SET"})
        String stringRepoType;

        InMemoryRepository<Order> repository;
        List<Order> orders;

        @Setup(Level.Trial)
        public void doSetup() {
            RepositoryType repoType = RepositoryType.valueOf(stringRepoType);

            switch (repoType) {
                case ARRAY_LIST:
                    repository = new ArrayListBasedRepository<>();
                    break;
                case HASH_SET:
                    repository = new HashSetBasedRepository<>();
                    break;
                case TREE_SET:
                    repository = new TreeSetBasedRepository<>();
                    break;
                case CONCURRENT_HASH_MAP:
                    repository = new ConcurrentHashMapBasedRepository<>();
                    break;
                case EC_FAST_LIST:
                    repository = new EcFastListBasedRepository<>();
                    break;
                case KOLOBOKE_HASH_MAP:
                    repository = new KolobokeHashIntObjectBasedRepository<>();
                    break;
                case FU_ARRAY_SET:
                    repository = new FUArraySetBasedRepository<>();
                    break;
                case TROVE_HASH_SET:
                    repository = new TroveHashSetBasedRepository<>();
                    break;
            }


            orders = new ArrayList<>();

            Random random = new Random();
            for (int i = 0; i < 1000; i++) {
                Order o = new Order(i, random.nextInt(100), random.nextInt(100));
                orders.add(o);
                repository.add(o);
            }
        }

    }

    @State(Scope.Benchmark)
    public static class FullRepoStateRemove {
        @Param({"ARRAY_LIST", "HASH_SET", "TREE_SET", "CONCURRENT_HASH_MAP", "EC_FAST_LIST", "KOLOBOKE_HASH_MAP", "FU_ARRAY_SET", "TROVE_HASH_SET"})
        String stringRepoType;

        InMemoryRepository<Order> repository;
        List<Order> orders;

        @Setup(Level.Invocation)
        public void doSetup() {
            RepositoryType repoType = RepositoryType.valueOf(stringRepoType);

            switch (repoType) {
                case ARRAY_LIST:
                    repository = new ArrayListBasedRepository<>();
                    break;
                case HASH_SET:
                    repository = new HashSetBasedRepository<>();
                    break;
                case TREE_SET:
                    repository = new TreeSetBasedRepository<>();
                    break;
                case CONCURRENT_HASH_MAP:
                    repository = new ConcurrentHashMapBasedRepository<>();
                    break;
                case EC_FAST_LIST:
                    repository = new EcFastListBasedRepository<>();
                    break;
                case KOLOBOKE_HASH_MAP:
                    repository = new KolobokeHashIntObjectBasedRepository<>();
                    break;
                case FU_ARRAY_SET:
                    repository = new FUArraySetBasedRepository<>();
                    break;
                case TROVE_HASH_SET:
                    repository = new TroveHashSetBasedRepository<>();
                    break;
            }


            orders = new ArrayList<>();

            Random random = new Random();
            for (int i = 0; i < 10000; i++) {
                Order o = new Order(i, random.nextInt(100), random.nextInt(100));
                orders.add(o);
                repository.add(o);
            }
        }

    }

    @Benchmark
    public void addBenchmark(EmptyRepoState fullRepoState, Blackhole consumer) {
        for (Order order : fullRepoState.orders) {
            fullRepoState.repository.add(order);
            consumer.consume(order);
        }

        consumer.consume(fullRepoState.repository);
    }

    @Benchmark
    public void containsBenchmark(FullRepoStateContains fullRepoState, Blackhole consumer) {
        for (Order order : fullRepoState.orders)
            consumer.consume(fullRepoState.repository.contains(order));
    }


    @Benchmark
    public void removeBenchmark(FullRepoStateRemove fullRepoState, Blackhole consumer) {
        for (Order order : fullRepoState.orders) {
            fullRepoState.repository.remove(order);
        }

        consumer.consume(fullRepoState.repository);
    }
}
