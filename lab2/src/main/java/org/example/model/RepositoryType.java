package org.example.model;

public enum RepositoryType {
    ARRAY_LIST, HASH_SET, TREE_SET, CONCURRENT_HASH_MAP, EC_FAST_LIST, FU_ARRAY_SET, KOLOBOKE_HASH_MAP, TROVE_HASH_SET
}
