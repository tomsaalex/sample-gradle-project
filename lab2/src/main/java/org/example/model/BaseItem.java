package org.example.model;

public interface BaseItem {

    public int getId();

    public void setId(int id);
}
