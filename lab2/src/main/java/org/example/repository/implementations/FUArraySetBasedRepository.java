package org.example.repository.implementations;

import it.unimi.dsi.fastutil.objects.ObjectArraySet;
import org.example.repository.InMemoryRepository;

import java.util.Set;

public class FUArraySetBasedRepository<T> implements InMemoryRepository<T> {
    Set<T> set;

    public FUArraySetBasedRepository() {
        this.set = new ObjectArraySet<>();
    }

    @Override
    public void add(T element) {
        set.add(element);
    }

    @Override
    public boolean contains(T element) {
        return set.contains(element);
    }

    @Override
    public void remove(T element) {
        set.remove(element);
    }
}
