package org.example.repository.implementations;

import org.example.model.BaseItem;
import org.example.repository.InMemoryRepository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T extends BaseItem> implements InMemoryRepository<T> {

    private final Map<Integer, T> map;

    public ConcurrentHashMapBasedRepository() {
        this.map = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T element) {
        map.put(element.getId(), element);
    }

    @Override
    public boolean contains(T element) {
        return map.get(element.getId()) == element;
    }

    @Override
    public void remove(T element) {
        map.remove(element.getId());
    }
}
