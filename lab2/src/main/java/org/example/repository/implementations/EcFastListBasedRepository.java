package org.example.repository.implementations;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.list.mutable.FastList;
import org.example.repository.InMemoryRepository;

public class EcFastListBasedRepository<T> implements InMemoryRepository<T> {
    private final MutableList<T> list;



    public EcFastListBasedRepository() {
        this.list = new FastList<>();
    }

    @Override
    public void add(T element) {
        list.add(element);
    }

    @Override
    public boolean contains(T element) {
        return list.contains(element);
    }

    @Override
    public void remove(T element) {
        list.remove(element);
    }
}
