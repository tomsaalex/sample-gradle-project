package org.example.repository.implementations;

import gnu.trove.set.hash.THashSet;
import org.example.repository.InMemoryRepository;

import java.util.Set;

public class TroveHashSetBasedRepository<T> implements InMemoryRepository<T> {

    Set<T> set;

    public TroveHashSetBasedRepository() {
        this.set = new THashSet<>();
    }

    @Override
    public void add(T element) {
        set.add(element);
    }

    @Override
    public boolean contains(T element) {
        return set.contains(element);
    }

    @Override
    public void remove(T element) {
        set.remove(element);
    }
}
