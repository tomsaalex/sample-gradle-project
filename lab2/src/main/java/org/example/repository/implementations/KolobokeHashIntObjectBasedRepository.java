package org.example.repository.implementations;

import com.koloboke.collect.set.hash.HashObjSets;
import org.example.model.BaseItem;
import org.example.repository.InMemoryRepository;

import java.util.Set;

public class KolobokeHashIntObjectBasedRepository<T extends BaseItem> implements InMemoryRepository<T> {
    Set<T> set;

    public KolobokeHashIntObjectBasedRepository() {
        this.set = HashObjSets.getDefaultFactory().newMutableSet();
    }

    @Override
    public void add(T element) {
        set.add(element);
    }

    @Override
    public boolean contains(T element) {
        return set.contains(element);
    }

    @Override
    public void remove(T element) {
        set.remove(element);
    }
}
